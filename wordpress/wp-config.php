<?php

define('WP_ALLOW_REPAIR', true);
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '9&Dis)PO]JTD]RxW)v^+9{p&nY09KSG;r::d&gu@7pTgKQMBD(@M*9LP]RpSPue)');
define('SECURE_AUTH_KEY',  'i}*io-9m{1K`PgN6&~vyGsPf}r] {Zyg2_(/HQs}GKG;t`;K6hu5(zCIGj&Vq2;=');
define('LOGGED_IN_KEY',    'K!5/1n0nTgcVt&AYb{*/)K0},|@KvQ8G07Q9;5G[bMNI$VK?GzdT.Ic#Qn3g)<A+');
define('NONCE_KEY',        'dqKN.iE)(bdy5S ESAk>~eP^K9SUmRq!(8Ty$UG[xNG(<5H!shy-//|WqCZw[i>M');
define('AUTH_SALT',        'bU4!T7uT?7/BNv+*Fm]F3v5/HB!WQnLWZ?RRF]]~h(i2+cA c1vI])(h99W0!Gq(');
define('SECURE_AUTH_SALT', '^m4IINf0S)en/OW~X:C!B${*)/-Z(]fi&mJ@@{5/0>{kN6{>flSsv[g$1IrjcGi>');
define('LOGGED_IN_SALT',   '_9+LxaGHckk`6vH[uy5CO[iJUhUlY):h7EFgOk{VjG6P#gln8s<O2h[C= P2:co>');
define('NONCE_SALT',       'X+<90sClW:P-Eq9U|Y^Y%Z71s`9Oeiw&:>gosfy!~qUL?dYj6+@|sg?F{iJ+sPOq');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');


