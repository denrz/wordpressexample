<?php
/**
 * Displays footer site info
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<div class="site-info">
	<?php printf( 'Copyright © 2017 Denis Ryazanov Photography' ); ?>
</div><!-- .site-info -->
